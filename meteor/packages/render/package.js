Package.describe({
    name: 'savi:render',
    version: '0.0.1',
    // Brief, one-line summary of the package.
    summary: '',
    // URL to the Git repository containing the source code for this package.
    git: '',
    // By default, Meteor will default to using README.md for documentation.
    // To avoid submitting documentation, set this field to null.
    documentation: 'README.md'
});

Package.onUse(function(api) {
    api.versionsFrom('1.2.0.2');
    api.use('ecmascript');
    api.use('reactive-var');
    api.use('reactive-dict');
    api.use('templating');
    api.use('peerlibrary:blaze-components');

    api.addFiles([
        'client/package_globals.js',

        'client/body.html',
        'client/layout.js',
        'client/layout_wrapper.html',
        'client/layout_wrapper.js',
        'client/region_helper.js'
    ],'client');

    api.export('Layout');
});

Package.onTest(function(api) {
    api.use('ecmascript');
    api.use('tinytest');
    api.use('savi:render');
    api.addFiles('render-tests.js');
});
