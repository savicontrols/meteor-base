/* globals PackageGlobals: true */

/* name space to store package global variables */
PackageGlobals = {
    currentLayout : new ReactiveVar('', strictEquals),  // Name of the template passed to Layout.render
    currentRegions: new ReactiveDict()
};

function strictEquals(a,b) {
    return a === b;
}
