/* globals PackageGlobals: false */
'use strict';

BlazeComponent.extendComponent({
    getCurrentLayout: function() {
        return PackageGlobals.currentLayout.get();
    },

    getCurrentRegions: function() {
        return PackageGlobals.currentRegions.get();
    }
}).register('LayoutWrapper');
