/* globals Layout: true */
/* globals PackageGlobals: false */
'use strict';

Layout = {
    render: function(componentOrTemplate, regions) {
        var region, results, value;

        PackageGlobals.currentLayout.set(componentOrTemplate);
        results = [];
        for (region in regions) {
            if (regions.hasOwnProperty(region)) {
                value = regions[region];
                results.push(PackageGlobals.currentRegions.set(region, value));
            }
        }

        return results;
    },
};
