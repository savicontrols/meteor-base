/* globals PackageGlobals: false */
'use strict';

Template.registerHelper('region', function(region) {
    return {
        template: PackageGlobals.currentRegions.get(region)
    };
});
