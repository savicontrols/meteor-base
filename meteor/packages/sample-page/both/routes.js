'use strict';

FlowRouter.route('/', {
    action: function(/* params */) {
        Layout.render('BorderLayout', {
            north: 'SampleHeader',
            middle: 'SampleContent',
            south: 'CopyrightFooter'
        });
    }
});
