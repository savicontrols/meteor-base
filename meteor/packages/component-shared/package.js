Package.describe({
    name: 'savi:component-shared',
    version: '0.0.1',
    // Brief, one-line summary of the package.
    summary: '',
    // URL to the Git repository containing the source code for this package.
    git: '',
    // By default, Meteor will default to using README.md for documentation.
    // To avoid submitting documentation, set this field to null.
    documentation: 'README.md'
});

Package.onUse(function(api) {
    api.versionsFrom('1.2.0.2');
    api.use('ecmascript');
    api.use('templating');
    api.use('stylus');
    api.use('peerlibrary:blaze-components');

    // New Meteor 1.2 sylus importing
    api.addFiles([
        'client/index.styl'
    ], 'client', {isImport: true});

    // Client imports
    api.addFiles([
        'client/components/copyright-footer/copyright_footer.html',
        'client/components/copyright-footer/copyright_footer.import.styl',
        'client/components/copyright-footer/copyright_footer.js'
    ],'client');
});

Package.onTest(function(api) {
    api.use('ecmascript');
    api.use('tinytest');
    api.use('savi:component-shared');
    api.addFiles('component-shared-tests.js');
});
